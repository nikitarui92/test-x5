import { UserService } from '../components/user/index.js'
import { JWTTokenUtil } from '../utils/index.js'
import {Config} from '../config.js'

export class AuthMiddleware {

    constructor() {
        this.userService = new UserService()
    }

    middleware() {
        return async (ctx, next) => {
            
            const { authorization } = ctx.headers

            if (!authorization) {
                ctx.throw(400, 'authorization header is not set')
            }

            const token = authorization.split(' ')[1]

            if (!token) {
                ctx.throw(401)
            }

            const { sub } = JWTTokenUtil.decodeToken(token, Config.jwtSecret)
            const user = await this.userService.getById(sub)

            ctx.state.currentUser = user

            return next()
        }
    }

}

