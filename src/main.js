import Koa from "koa"
import koaBody from "koa-body";
import Router from '@koa/router'

import { AuthMiddleware } from './middlewares/index.js'
import { AuthController } from './components/auth/index.js'
import { UserController } from "./components/user/index.js";

const app = new Koa();
const router = new Router({ prefix: '/api' })

const auth = new AuthMiddleware()

router.use(new AuthController().routes())
router.use(
    auth.middleware(),
    new UserController().routes()
)

app.use(koaBody())
app.use(async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        const resErrorBody = {
            status: 500,
            message: 'Internal server error',
            properties: {}
        }
        if (err.status < 500) {
            resErrorBody.status = err.status
            resErrorBody.message = err.message
            resErrorBody.properties = err.properties
        }
        ctx.status = resErrorBody.status
        ctx.body = resErrorBody
        console.error(err)
        ctx.app.emit('error', err, ctx);
    }
});
app.use(router.routes());
app.use(router.allowedMethods())

app.listen(3000);
