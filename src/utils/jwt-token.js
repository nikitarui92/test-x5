import jwt from 'jsonwebtoken'

export class InvalidToken extends Error{
    status = 400
    message = 'invalid token'
    name = this.constructor.name
}

export class JWTTokenUtil {

    static genToken(payload, jwtSecret) {
        return jwt.sign(payload, jwtSecret)
    }

    static decodeToken(token, jwtSecret) {
        try {
            return jwt.verify(token, jwtSecret)
        } catch (err) {
            if(err instanceof jwt.JsonWebTokenError){
                throw new InvalidToken()
            }
            throw err
        }
    }

}
