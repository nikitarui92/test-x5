import * as crypto from 'crypto'

export class CryptoUtil {
    
    static sha256(data){
        return crypto.createHash('sha256').update(data, 'utf8').digest().toString('base64')
    }

}
