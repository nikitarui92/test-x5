import LoginReqDto from '../components/auth/dto/loginReqDto.schema.json'
import UserDto from '../components/user/dto/userDto.schema.json'

export const DtoRegistry = {

    LoginReqDto: {
        schema: LoginReqDto,
        name: 'LoginReqDto'
    },
    UserDto: {
        schema: UserDto,
        name: 'UserDto'
    }

}