import Ajv from 'ajv'
import { DtoRegistry } from './dto-registry.js'

export class ValidationError extends Error{
    status = 400
    message='validation error'
    name = this.constructor.name

    constructor(properties={}){
        super()
        this.properties = properties
    }
}

export class DtoValidator {

    static #instance

    constructor() {
        this.ajv = new Ajv()
        this.ajv.addSchema(DtoRegistry.LoginReqDto.schema, DtoRegistry.LoginReqDto.name)
        this.ajv.addSchema(DtoRegistry.UserDto.schema, DtoRegistry.UserDto.name)
    }

    static get instance() {
        if (!DtoValidator.#instance) {
            DtoValidator.#instance = new DtoValidator()
        }
        return DtoValidator.#instance
    }

    validate(dtoName, data) {
        const valid = this.ajv.validate(dtoName, data)

        if(!valid){
            throw new ValidationError({
                errors: this.ajv.errors
            })
        }

    }

}
