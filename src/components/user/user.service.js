import { db } from '../../../db.js'
import { UserNotFound } from './user.errors.js'

export class UserService {

    constructor() { }

    async getByEmail(email) {
        const user = db.users.find(user => user.email == email)

        if (!user) {
            throw new UserNotFound()
        }

        return user
    }

    async getById(id) {
        const user = db.users.find(user => user.id == id)

        if (!user) {
            throw new UserNotFound()
        }

        return user
    }

}
