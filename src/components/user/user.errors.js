
export class UserNotFound extends Error{
    status = 404
    message = 'user not found'
}
