import Router from '@koa/router'

import { DtoValidator } from '../../services/dto-validator.js'
import { DtoRegistry } from '../../services/dto-registry.js'
import { UserService } from './user.service.js'

export class UserController {

    constructor() {
        this.router = new Router({prefix: '/users'})
        this.validator = DtoValidator.instance
        this.userService = new UserService()
    }

    async getCurrent(ctx) {
        const currentUser = ctx.state.currentUser
        ctx.body = {
            id: currentUser.id,
            email: currentUser.email,
        }
    }

    routes() {
        this.router.get('/current', this.getCurrent.bind(this))
        return this.router.routes()
    }

}
