import {CryptoUtil, JWTTokenUtil} from '../../utils/index.js'
import {PasswordInvalid} from './auth.errors.js'
import {UserService} from '../user/index.js'
import {Config} from '../../config.js'

export class AuthService {

    constructor(){
        this.userService = new UserService()
    }

    async login({email, password}){

        const user = await this.userService.getByEmail(email)
      
        const passHash = CryptoUtil.sha256(password)
      
        if (user.password != passHash){
          throw new PasswordInvalid()
        }
      
        const token = JWTTokenUtil.genToken({
          sub: user.id
        }, Config.jwtSecret)
      
        return {
            token
        }

    }

}
