
export class PasswordInvalid extends Error {
    status = 400
    message = 'password invalid'
    name = this.constructor.name
}
