import Router from '@koa/router'

import { DtoValidator } from '../../services/dto-validator.js'
import { DtoRegistry } from '../../services/dto-registry.js'
import { AuthService } from './auth.service.js'


export class AuthController {

    constructor() {
        this.router = new Router()
        this.validator = DtoValidator.instance
        this.authService = new AuthService()
    }

    async login(ctx) {
        const body = ctx.request.body
        this.validator.validate(DtoRegistry.LoginReqDto.name, body)

        const { email, password } = body

        const res = await this.authService.login({email, password})
        
        ctx.body = res
        
    }

    routes() {
        this.router.post('/login', this.login.bind(this))
        return this.router.routes()
    }

}
